package ru.t1.godyna.tm.controller;

import ru.t1.godyna.tm.api.ITaskService;
import ru.t1.godyna.tm.model.Task;
import ru.t1.godyna.tm.util.TerminalUtil;

import java.util.List;

public final class TaskController implements ru.t1.godyna.tm.api.ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService ITaskService) {
        this.taskService = ITaskService;
    }

    @Override
    public void createTask() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) System.out.println("[ERROR]");
        else System.out.println("[OK]");
    }

    @Override
    public void showTasks() {
        System.out.println("[TASK LIST]");
        int index = 1;
        final List<Task> tasks = taskService.findAll();
        for (final Task task: tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getName());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTask() {
        System.out.println("[TASKS CLEAR]");
        taskService.clear();
        System.out.println("[OK]");
    }

}
