package ru.t1.godyna.tm.component;

import ru.t1.godyna.tm.api.*;
import ru.t1.godyna.tm.constant.ArgumentConst;
import ru.t1.godyna.tm.constant.CommandConst;
import ru.t1.godyna.tm.controller.CommandController;
import ru.t1.godyna.tm.controller.ProjectController;
import ru.t1.godyna.tm.controller.TaskController;
import ru.t1.godyna.tm.repository.CommandRepository;
import ru.t1.godyna.tm.repository.ProjectRepository;
import ru.t1.godyna.tm.repository.TaskRepository;
import ru.t1.godyna.tm.service.CommandService;
import ru.t1.godyna.tm.service.ProjectService;
import ru.t1.godyna.tm.service.TaskService;
import ru.t1.godyna.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    public void start(final String[] args) {
        processArguments(args);
        System.out.println("** WELCOME TO TASK MANAGER **");
        while(!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            processCommand(TerminalUtil.nextLine());
        }
    }

    private void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        System.exit(0);
    }

    private void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
            break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
            break;
            case ArgumentConst.HELP:
                commandController.showHelp();
            break;
            case ArgumentConst.INFO:
                commandController.showInfo();
            break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
            break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
            break;
            default:
                commandController.showArgumentError();
            break;
        }
    }

    private void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConst.ABOUT:
                commandController.showAbout();
            break;
            case CommandConst.VERSION:
                commandController.showVersion();
            break;
            case CommandConst.HELP:
                commandController.showHelp();
            break;
            case CommandConst.INFO:
                commandController.showInfo();
            break;
            case CommandConst.COMMANDS:
                commandController.showCommands();
            break;
            case CommandConst.ARGUMENTS:
                commandController.showArguments();
            break;
            case CommandConst.PROJECT_CREATE:
                projectController.createProject();
            break;
            case CommandConst.PROJECT_LIST:
                projectController.showProjects();
            break;
            case CommandConst.PROJECT_CLEAR:
                projectController.clearProjects();
            break;
            case CommandConst.TASK_CREATE:
                taskController.createTask();
            break;
            case CommandConst.TASK_LIST:
                taskController.showTasks();
            break;
            case CommandConst.TASK_CLEAR:
                taskController.clearTask();
            break;
            case CommandConst.EXIT:
                exit();
            break;
            default:
                commandController.showCommandError();
            break;
        }
    }

    private void exit() {
        System.exit(0);
    }

}
