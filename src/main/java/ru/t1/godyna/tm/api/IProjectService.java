package ru.t1.godyna.tm.api;

import ru.t1.godyna.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

}
