package ru.t1.godyna.tm.api;

import ru.t1.godyna.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    List<Task> findAll();

    void clear();

}
