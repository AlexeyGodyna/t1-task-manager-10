package ru.t1.godyna.tm.api;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTask();

}
