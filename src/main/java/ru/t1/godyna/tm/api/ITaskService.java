package ru.t1.godyna.tm.api;

import ru.t1.godyna.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);

}
