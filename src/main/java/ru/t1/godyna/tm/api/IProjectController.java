package ru.t1.godyna.tm.api;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

}
